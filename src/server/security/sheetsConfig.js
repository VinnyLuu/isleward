module.exports = {
	sheetId: '',

	roles: [{
		username: 'waffle',
		level: 10,
		messagestyle: 'color-blueA',
		messageprefix: '(dev) ',
		skins: ['*'],
		items: [{
			type: 'key',
			name: 'Key to the world',
			sprite: [12, 0],
			keyId: 'world',
			noSalvage: true,
			noDrop: true,
			noDestroy: true
		}],
		extrastashslots: 10
	}]
};
